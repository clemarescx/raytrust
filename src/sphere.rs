use crate::hitable::*;
use crate::material::Surface;
use crate::ray::Ray;
use crate::Vec3;
use std::cell::RefCell;

pub struct Sphere {
    center: Vec3,
    radius: f64,
    mat_ptr: Surface,
}

impl Sphere {
    pub fn new(center: Vec3, radius: f64, mat_ptr: Surface) -> Self {
        Sphere {
            center,
            radius,
            mat_ptr,
        }
    }
}

impl Hitable for Sphere {
    fn hit(&self, r: &Ray, t_min: f64, t_max: f64, rec: &RefCell<HitRecord>) -> bool {
        let oc = r.origin() - self.center;
        let a = r.direction().dot(&r.direction());
        let b = oc.dot(&r.direction());
        let c = oc.dot(&oc) - self.radius * self.radius;
        let discriminant = b * b - a * c;

        if discriminant > 0.0 {
            let discriminant_sqrt = discriminant.sqrt();
            let temp = (-b - discriminant_sqrt) / a;
            if temp < t_max && temp > t_min {
                let mut rec = rec.borrow_mut();
                rec.t = temp;
                rec.p = r.point_at_parameter(rec.t);
                rec.normal = (rec.p - self.center) / self.radius;
                rec.mat_ptr = self.mat_ptr;

                return true;
            }

            let temp = (-b + discriminant_sqrt) / a;
            if temp < t_max && temp > t_min {
                let mut rec = rec.borrow_mut();
                rec.t = temp;
                rec.p = r.point_at_parameter(rec.t);
                rec.normal = (rec.p - self.center) / self.radius;
                rec.mat_ptr = self.mat_ptr;
                return true;
            }
        }

        false
    }
}
