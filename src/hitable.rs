use crate::material::Surface;
use crate::ray::Ray;
use nalgebra::Vector3;
use std::cell::RefCell;

type Vec3 = Vector3<f64>;

#[derive(Clone, Copy)]
pub struct HitRecord {
    pub t: f64,
    pub p: Vec3,
    pub normal: Vec3,
    pub mat_ptr: Surface,
}

impl HitRecord {
    pub fn new() -> Self {
        HitRecord {
            t: 0.0,
            p: Vec3::zeros(),
            normal: Vec3::zeros(),
            mat_ptr: Surface::None,
        }
    }
}

pub trait Hitable {
    fn hit(&self, r: &Ray, t_min: f64, t_max: f64, rec: &RefCell<HitRecord>) -> bool;
}

pub struct HitableList<'a, T: Hitable> {
    pub list: &'a [T],
}

impl<'a, T> HitableList<'a, T>
where
    T: Hitable,
{
    pub fn new(list: &'a [T]) -> HitableList<'a, T> {
        HitableList { list }
    }
}

impl<'a, T> Hitable for HitableList<'a, T>
where
    T: Hitable,
{
    fn hit(&self, r: &Ray, t_min: f64, t_max: f64, rec: &RefCell<HitRecord>) -> bool {
        let temp_rec = rec.clone();
        let mut hit_anything = false;
        let mut closest_so_far = t_max;
        for hl in self.list.iter() {
            if hl.hit(r, t_min, closest_so_far, &temp_rec) {
                hit_anything = true;
                closest_so_far = temp_rec.borrow().t;
                rec.replace(*temp_rec.borrow());
            }
        }
        hit_anything
    }
}
