use crate::hitable::Hitable;
use nalgebra::Vector3;
use rand::Rng;
use rayon::prelude::*;
use std::fs::File;
use std::io::prelude::*;
use std::io::*;

mod camera;
mod hitable;
mod material;
mod ray;
mod sphere;

use camera::Camera;
use hitable::{HitRecord, HitableList};
use material::*;
use ray::*;
use sphere::Sphere;
use std::cell::RefCell;
use std::time::SystemTime;

type Vec3 = Vector3<f64>;
const MAXFLOAT: f64 = std::f64::MAX;

fn color(r: &Ray, world: &Hitable, depth: f64) -> Vec3 {
    let rec = RefCell::new(HitRecord::new());
    if world.hit(r, 0.001, MAXFLOAT, &rec) {
        let rec = rec.borrow();
        let mut scattered = Ray::new(&Vec3::zeros(), &Vec3::zeros());
        let mut attenuation = Vec3::zeros();

        if depth < 50.0
            && rec
                .mat_ptr
                .scatter(r, &rec, &mut attenuation, &mut scattered)
        {
            color(&scattered, world, depth + 1.0).component_mul(&attenuation)
        } else {
            Vec3::zeros()
        }
    } else {
        let unit_direction = r.direction().normalize();
        let t = 0.5 * (unit_direction.y + 1.0);
        (1.0 - t) * Vec3::repeat(1.0) + t * Vec3::new(0.5, 0.7, 1.0)
    }
}

fn main() -> Result<()> {
    let timer = SystemTime::now();
    let nx = 200;
    let ny = 100;
    // let ns = 1 << 7;
    let ns = 1 << 7;

    let ppm_header = format!("P3\n{} {}\n255\n", nx, ny);
    let mut file_content = ppm_header;

    let look_from = Vec3::new(13.0, 2.0, 3.0);
    let look_at = Vec3::new(0.0, 0.0, 0.0);
    let dist_to_focus = 10.0;
    let aperture = 0.1;

    let cam = Camera::new(
        look_from,
        look_at,
        Vec3::new(0.0, 1.0, 0.0),
        20.0,
        f64::from(nx) / f64::from(ny),
        aperture,
        dist_to_focus,
    );

    // let list = default_spheres();
    println!("Generate world...");
    let list = random_scene();

    let world = HitableList::new(&list);

    println!("Start tracing...");
    for j in (0..ny).rev() {
        for i in 0..nx {
            // let mut col = Vec3::zeros();

            // for _ in 0..ns {
            //     let u = (f64::from(i) + rng.gen::<f64>()) / f64::from(nx);
            //     let v = (f64::from(j) + rng.gen::<f64>()) / f64::from(ny);
            //     let r = cam.get_ray(u, v);
            //     // let p = r.point_at_parameter(2.0);
            //     col += color(&r, &world, 0.0);
            // }
            let mut col = (0..ns)
                .into_par_iter()
                .map(|_| {
                    let mut rng = rand::thread_rng();
                    let u = (f64::from(i) + rng.gen::<f64>()) / f64::from(nx);
                    let v = (f64::from(j) + rng.gen::<f64>()) / f64::from(ny);
                    let r = cam.get_ray(u, v);
                    // let p = r.point_at_parameter(2.0);
                    color(&r, &world, 0.0)
                })
                .sum::<Vec3>();
            // .fold(|| Vec3::zeros(), |mut acc, curr| acc + curr);
            col /= f64::from(ns);
            col = col.map(|c| c.sqrt());

            let ir = (255.99 * col.x) as i32;
            let ig = (255.99 * col.y) as i32;
            let ib = (255.99 * col.z) as i32;
            file_content.push_str(&format!("{} {} {}\n", ir, ig, ib));
        }
    }

    let elapsed = timer.elapsed().unwrap();
    let elapsed = elapsed.as_millis() as f64 * 1.0e-3f64;

    println!("Total tracing time: {}", elapsed);

    let mut file = File::create("colours.ppm")?;
    file.write_all(file_content.as_bytes())?;

    Ok(())
}

fn random_scene() -> Vec<Sphere> {
    let n = 500;
    let mut list = Vec::with_capacity(n + 1);

    let first = Sphere::new(
        Vec3::new(0.0, -1000.0, 0.0),
        1000.0,
        Surface::Lambertian {
            albedo: Vec3::repeat(0.5),
        },
    );
    list.push(first);

    let mut rng = rand::thread_rng();

    for a in -11..11 {
        for b in -11..11 {
            let x_cell = f64::from(a);
            let y_cell = f64::from(b);
            let center = Vec3::new(
                x_cell + 0.9 * rng.gen::<f64>(),
                0.2,
                y_cell + 0.9 * rng.gen::<f64>(),
            );
            if (center - Vec3::new(4.0, 0.2, 0.0)).norm() > 0.9 {
                let material = match rng.gen::<f64>() {
                    x if x < 0.8 => Surface::Lambertian {
                        albedo: Vec3::new(
                            rng.gen::<f64>() * rng.gen::<f64>(),
                            rng.gen::<f64>() * rng.gen::<f64>(),
                            rng.gen::<f64>() * rng.gen::<f64>(),
                        ),
                    },
                    x if x < 0.95 => Surface::Metal {
                        albedo: Vec3::new(
                            0.5 * (1.0 + rng.gen::<f64>()),
                            0.5 * (1.0 + rng.gen::<f64>()),
                            0.5 * (1.0 + rng.gen::<f64>()),
                        ),
                        fuzz: (0.5 * rng.gen::<f64>()).into(),
                    },
                    _ => Surface::Dielectric { ref_idx: 1.5 },
                };
                let current = Sphere::new(center, 0.2, material);
                list.push(current);
            }
        }
    }

    let mut bigguns = vec![
        Sphere::new(
            Vec3::new(0.0, 1.0, 0.0),
            1.0,
            Surface::Dielectric { ref_idx: 1.5 },
        ),
        Sphere::new(
            Vec3::new(-4.0, 1.0, 0.0),
            1.0,
            Surface::Lambertian {
                albedo: Vec3::new(0.4, 0.2, 0.1),
            },
        ),
        Sphere::new(
            Vec3::new(4.0, 1.0, 0.0),
            1.0,
            Surface::Metal {
                albedo: Vec3::new(0.7, 0.6, 0.5),
                fuzz: 0.0.into(),
            },
        ),
    ];
    list.append(&mut bigguns);

    list
}

#[allow(dead_code)]
fn default_spheres() -> Vec<Sphere> {
    let materials = vec![
        Surface::Lambertian {
            albedo: Vec3::new(0.1, 0.2, 0.5),
        },
        Surface::Lambertian {
            albedo: Vec3::new(0.8, 0.8, 0.0),
        },
        Surface::Metal {
            albedo: Vec3::new(0.8, 0.6, 0.2),
            fuzz: 0.1.into(),
        },
        Surface::Dielectric { ref_idx: 1.5 },
        Surface::Lambertian {
            albedo: Vec3::new(0.0, 0.0, 1.0),
        },
        Surface::Lambertian {
            albedo: Vec3::new(1.0, 0.0, 0.0),
        },
    ];

    vec![
        Sphere::new(Vec3::new(0.0, 0.0, -1.0), 0.5, materials[0]),
        Sphere::new(Vec3::new(0.0, -100.5, -1.0), 100.0, materials[1]),
        Sphere::new(Vec3::new(1.0, 0.0, -1.0), 0.5, materials[2]),
        Sphere::new(Vec3::new(-1.0, 0.0, -1.0), 0.5, materials[3]),
        Sphere::new(Vec3::new(-1.0, 0.0, -1.0), -0.45, materials[3]),
    ]
}
