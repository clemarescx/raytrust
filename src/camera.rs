use crate::ray::Ray;
use nalgebra::Vector3;
use rand::Rng;

type Vec3 = Vector3<f64>;

pub struct Camera {
    pub origin: Vec3,
    pub lower_left_corner: Vec3,
    pub horizontal: Vec3,
    pub vertical: Vec3,
    pub u: Vec3,
    pub v: Vec3,
    pub w: Vec3,
    pub lens_radius: f64,
}

impl Camera {
    pub fn new(
        look_from: Vec3,
        look_at: Vec3,
        vup: Vec3,
        v_fov: f64,
        aspect: f64,
        aperture: f64,
        focus_dist: f64,
    ) -> Self {
        let theta = v_fov * std::f64::consts::PI / 180.0;
        let half_height = (theta / 2.0).tan();
        let half_width = aspect * half_height;
        let w = (look_from - look_at).normalize();
        let u = (vup.cross(&w)).normalize();
        let v = w.cross(&u);
        Camera {
            origin: look_from,
            // lower_left_corner: Vec3::new(-half_width, -half_height, -1.0),
            lower_left_corner: look_from
                - half_width * focus_dist * u
                - half_height * focus_dist * v
                - focus_dist * w,
            // horizontal: Vec3::new(2.0 * half_width, 0.0, 0.0),
            horizontal: 2.0 * half_width * focus_dist * u,
            // vertical: Vec3::new(0.0, 2.0 * half_height, 0.0),
            vertical: 2.0 * half_height * focus_dist * v,
            lens_radius: aperture / 2.0,
            u,
            v,
            w,
        }
    }

    pub fn get_ray(&self, s: f64, t: f64) -> Ray {
        let rd = self.lens_radius * random_in_unit_disk();
        let offset = self.u * rd.x + self.v * rd.y;
        Ray::new(
            &(self.origin + offset),
            &(self.lower_left_corner + s * self.horizontal + t * self.vertical
                - self.origin
                - offset),
        )
    }
}

fn random_in_unit_disk() -> Vec3 {
    let mut rng = rand::thread_rng();
    loop {
        let p = 2.0 * Vec3::new(rng.gen::<f64>(), rng.gen::<f64>(), 0.0)
            - Vec3::new(1.0, 1.0, 0.0);
        if p.dot(&p) >= 1.0 {
            break p;
        }
    }
}
