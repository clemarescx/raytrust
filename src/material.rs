use crate::hitable::HitRecord;
use crate::ray::Ray;
use nalgebra::Vector3;
use rand::Rng;

type Vec3 = Vector3<f64>;

#[derive(Clone, Copy)]
pub struct FuzzIndex(f64);
impl FuzzIndex {
    fn new(fuzz: f64) -> Self {
        FuzzIndex(if fuzz < 1.0 { fuzz } else { 1.0 })
    }
}

impl Into<FuzzIndex> for f64 {
    fn into(self) -> FuzzIndex {
        FuzzIndex::new(self)
    }
}

impl From<FuzzIndex> for f64 {
    fn from(f: FuzzIndex) -> Self {
        f.0
    }
}

#[derive(Clone, Copy)]
pub enum Surface {
    Lambertian { albedo: Vec3 },
    Metal { albedo: Vec3, fuzz: FuzzIndex },
    Dielectric { ref_idx: f64 },
    None,
}

impl Material for Surface {
    fn scatter(
        &self,
        r_in: &Ray,
        rec: &HitRecord,
        attenuation: &mut Vec3,
        scattered: &mut Ray,
    ) -> bool {
        match self {
            Surface::Lambertian { albedo } => {
                let target = rec.p + rec.normal + random_in_unit_sphere();
                *scattered = Ray::new(&rec.p, &(target - rec.p));
                *attenuation = *albedo;
                true
            }
            Surface::Metal { albedo, fuzz } => {
                let reflected = reflect(&r_in.direction().normalize(), &rec.normal);
                *scattered = Ray::new(&rec.p, &(reflected + fuzz.0 * random_in_unit_sphere()));
                *attenuation = *albedo;
                scattered.direction().dot(&rec.normal) > 0.0
            }
            Surface::Dielectric { ref_idx } => {
                let ref_idx = *ref_idx;
                let reflected = reflect(&r_in.direction(), &rec.normal);
                *attenuation = Vec3::repeat(1.0);

                let mut refracted = Vec3::repeat(1.0);

                let (outward_normal, ni_over_nt, cosine) = if r_in.direction().dot(&rec.normal)
                    > 0.0
                {
                    let c = ref_idx * r_in.direction().dot(&rec.normal) / r_in.direction().norm();
                    (-rec.normal, ref_idx, c)
                } else {
                    let c = -r_in.direction().dot(&rec.normal) / r_in.direction().norm();
                    (rec.normal, 1.0 / ref_idx, c)
                };

                let reflect_prob = if refract(
                    &r_in.direction(),
                    &outward_normal,
                    ni_over_nt,
                    &mut refracted,
                ) {
                    schlick(cosine, ref_idx)
                } else {
                    1.0
                };

                *scattered = if rand::thread_rng().gen_range(0.0, 1.0) < reflect_prob {
                    Ray::new(&rec.p, &reflected)
                } else {
                    Ray::new(&rec.p, &refracted)
                };

                true
            }

            Surface::None => false,
        }
    }
}

pub trait Material {
    fn scatter(
        &self,
        r_in: &Ray,
        rec: &HitRecord,
        attenuation: &mut Vec3,
        scattered: &mut Ray,
    ) -> bool;
}

fn schlick(cosine: f64, ref_idx: f64) -> f64 {
    let r0 = (1.0 - ref_idx) / (1.0 + ref_idx);
    let r0 = r0 * r0;
    r0 + (1.0 - r0) * (1.0 - cosine).powf(5.0)
}

fn refract(v: &Vec3, n: &Vec3, ni_over_nt: f64, refracted: &mut Vec3) -> bool {
    let uv = v.normalize();
    let dt = uv.dot(n);
    let discriminant = 1.0 - ni_over_nt * ni_over_nt * (1.0 - dt * dt);
    if discriminant > 0.0 {
        *refracted = ni_over_nt * (uv - n * dt) - n * discriminant.sqrt();
        true
    } else {
        false
    }
}

fn reflect(v: &Vec3, n: &Vec3) -> Vec3 {
    v - 2.0 * v.dot(n) * n
}

fn random_in_unit_sphere() -> Vec3 {
    let mut rng = rand::thread_rng();
    loop {
        let p =
            2.0 * Vec3::new(
                rng.gen_range(0.0, 1.0),
                rng.gen_range(0.0, 1.0),
                rng.gen_range(0.0, 1.0),
            ) - Vec3::repeat(1.0);
        if p.norm_squared() >= 1.0 {
            break p;
        }
    }
}
